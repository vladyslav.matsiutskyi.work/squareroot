package ua.work;

public class SquareRoot {
    public static void main(String[] args) {
        double number=0.25;
        double result = getSquareRoot(number);
        System.out.println(String.format(" %.15f", result));
    }
    public static double getSquareRoot(double number) {
        if (number < 0) {
            throw new IllegalArgumentException("Number must be non-negative");
        }

        double y = 1.0;
        double epsilon = 1e-4;

        while (true) {
            double newY = (number / y + y) / 2.0;
            if (Math.abs(newY - y) < epsilon) {
                break;
            }
            y = newY;
        }

        return y;
    }
}